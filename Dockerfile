FROM openjdk:8
EXPOSE 8090
ENV SPRING_PROFILE=h2
COPY target/assignment-*.jar /usr/local/app.jar
ENTRYPOINT java -jar -Dspring.profiles.active=${SPRING_PROFILE} /usr/local/app.jar
